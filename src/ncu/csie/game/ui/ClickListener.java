package ncu.csie.game.ui;

public interface ClickListener {
	
	public void onClick();
}
