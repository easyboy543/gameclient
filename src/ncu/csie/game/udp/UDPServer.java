package ncu.csie.game.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.swing.SwingUtilities;

import ncu.csie.game.Model;

public class UDPServer implements Runnable{

	public static final int LISTEN_PORT = 8888;	
	private Model model;
	
	public UDPServer(Model model){
		this.model = model;
	}
	
	
	public void listenRequest(){
		try{			
			DatagramSocket serverSocket = new DatagramSocket(LISTEN_PORT);

			byte[] receiveData = new byte[1024];			

			while (true) {
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				serverSocket.receive(receivePacket);
				String serverData = new String(receivePacket.getData()).trim();
				InetAddress IPAddress = receivePacket.getAddress();
				int port = receivePacket.getPort();
				int waitForNums = Integer.parseInt(serverData);
				model.setWiatForNums(waitForNums);				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		listenRequest();		
	}
		
}
